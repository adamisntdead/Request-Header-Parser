# Request Header Parser Microservice (FreeCodeCamp)
This is the backend project for FreeCodeCamp - See [Here](https://www.freecodecamp.com/challenges/request-header-parser-microservice)

## Usage
go to [https://fccheaderparse.herokuapp.com](fccheaderparse.herokuapp.com) and it will do its thing!

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## User Storys
 I can get the IP address, language and operating system for my browser.

## License
Released under GNU (General Public License)
Permission for individuals, Organizations and Companies to run, study, share (copy), and modify the software and its source.
Copyright Adam Kelly 2016-2017
