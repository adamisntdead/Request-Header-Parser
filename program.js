// Requires & Setup
var express = require('express');
var app = express();
var clientIp = require('client-ip');
var os = require("node.os");

// Variables
var port = process.env.PORT;
var obj = {
    "IP": "Unknown",
    "OS": "Unknown",
    "Language": "Unknown"
};


// Server Code
app.get("*", function(req, res) {

    // Edit JSON

    // IP
    obj.IP = clientIp(req);

    // Language
    var lang = req.acceptsLanguages('fr', 'es', 'en');
    if (lang) {
        obj.Language = lang;
    }
    else {
        obj.Language = "Null";
    }

    // OS
    obj.OS = req.headers['user-agent'].split(') ')[0].split(' (')[1];

    // Send To Client
    res.json(obj);
});
// Listen to specified port and print to console
app.listen(port, function() {
    console.log("listening to port: " + port);
})